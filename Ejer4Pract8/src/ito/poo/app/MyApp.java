package ito.poo.app;
import ito.poo.clases.CuerpoCeleste;
import ito.poo.clases.Ubicacion;
public class MyApp {
	static void run() {
		CuerpoCeleste c1= new CuerpoCeleste("Luna", null , "Solidos");
		System.out.println (c1);
		System.out.println ();

		Ubicacion u1 = new Ubicacion("12 horas",  41.2676200f, (float) 1.9733400, 384.400F);
		System.out.println (u1);
		c1.desplazamiento(0, 0);
		System.out.println ();
		
		System.out.println ();
		System.out.println ("/*********************************************************************/");
		System.out.println ();
		
		CuerpoCeleste c2= new CuerpoCeleste("Cometas",null, "Gas");
		System.out.println (c2);
		System.out.println ();

		Ubicacion u2 = new Ubicacion("6 meses", 21.87305556f, (float) 57.22333333, 12.05664F);
		System.out.println (u2);
		c2.desplazamiento(1, 789);
		
		System.out.println ();
		System.out.println ("/********************************************************************/");
		System.out.println ();
		
		CuerpoCeleste c3= new CuerpoCeleste("Luna", null , "Solidos");
		System.out.println (c3);
		System.out.println ();

		Ubicacion u3 = new Ubicacion("12 horas",  41.2676200f, (float) 1.9733400, 384.400F);
		System.out.println (u3);
		c3.desplazamiento(0, 0);
		System.out.println ();
		System.out.println ();
		System.out.println ();
		
		System.out.println(!c1.equals(c2));
	    System.out.println(c2.compareTo(c1));
	    
		System.out.println ();

	    System.out.println(!u1.equals(u2));
	    System.out.println(u2.compareTo(u1));
	    
	    System.out.println ();
	    
	    System.out.println(!c1.equals(c3));
	    System.out.println(c3.compareTo(c1));
	    
		System.out.println ();

	    System.out.println(!u1.equals(u3));
	    System.out.println(u3.compareTo(u1));
	}

		
	public static void main(String[] args) {
	run();	

	}
}
