package ito.poo.clases;
import java.util.HashSet;

public class CuerpoCeleste {
	
	private String nombre=  " ";
	private HashSet<Ubicacion> localizacion;
	private String composicion=  " ";
	
	public CuerpoCeleste() {
		super();
	}
	
	public CuerpoCeleste(String nombre, HashSet<Ubicacion> localizacion, String composicion) {
		super();
		this.nombre = nombre;
		this.localizacion = localizacion;
		this.composicion = composicion;
	}
	
	public float desplazamiento(int i, int j  ) {	
	float desplazamiento=0f;
	desplazamiento=(float) (Math.sqrt(Math.pow(i,2)+Math.pow(j, 2)));
	if(desplazamiento==0)
		System.out.println("-1");
	else
		System.out.println("El desplazamiento es de:"+desplazamiento);
			return (float) desplazamiento;
	}

	public float desplazamiento(int j ) {
	float  desplazamiento=j;
	return desplazamiento;
	}

	public HashSet<Ubicacion> getLocalizacion() {
		return this.localizacion;
	}

	public void setLocalizacion(HashSet<Ubicacion> localizacion) {
		this.localizacion = localizacion;
	}

	public String getComposicion() {
		return composicion;
	}

	public void setComposicion(String composicion) {
		this.composicion = composicion;
	}

	public String getNombre() {
		return nombre;
	}
	public String toString() {
		return "CuerpoCeleste [nombre=" + nombre + ", localizacion=" + localizacion + ", composicion=" + composicion + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((composicion == null) ? 0 : composicion.hashCode());
		result = prime * result + ((localizacion == null) ? 0 : localizacion.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CuerpoCeleste other = (CuerpoCeleste) obj;
		if (composicion == null) {
			if (other.composicion != null)
				return false;
		} else if (!composicion.equals(other.composicion))
			return false;
		if (localizacion == null) {
			if (other.localizacion != null)
				return false;
		} else if (!localizacion.equals(other.localizacion))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}
	public int compareTo(CuerpoCeleste arg0) {
		int r = 0;
		if (!this.nombre.equals(arg0.getNombre()))
			return this.nombre.compareTo(arg0.getNombre());
		else if (!this.composicion.equals(arg0.getComposicion()))
			return this.composicion.compareTo(arg0.getComposicion());
		return r;
	}	
}
