package ito.poo.clases;

import java.util.HashSet;

public class Ubicacion  {
	
	private String periodo=  " ";
	private float distancia= 0F;;
	private float longitud= 0F;;
	private float latitud= 0F;;
	private HashSet<Ubicacion> localizacion;

	public Ubicacion() {
		super();
	}
	
	public Ubicacion(String periodo, float distancia, float longitud, float latitud) {
		super();
		this.periodo = periodo;
		this.distancia = distancia;
		this.longitud = longitud;
		this.latitud = latitud;
	}

	public float getDistancia() {
		return distancia;
	}

	public void setDistancia(float distancia) {
		this.distancia = distancia;
	}

	public float getLongitud() {
		return this.longitud;
	}

	public float setLongitud() {
		return this.longitud;
	}

	public float getLatitud() {
		return this.latitud;
	}
	
	public float setLatitud() {
		return this.latitud;
	}

	public String getPeriodo() {
		return periodo;
	}
	
	public void desplazamiento(int i, int j) {		
	}

	public String toString() {
		return "Ubicacion [periodo=" + periodo + ", distancia= " + distancia + ", longitud=" + longitud + ", latitud=" + latitud + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(distancia);
		result = prime * result + Float.floatToIntBits(latitud);
		result = prime * result + ((localizacion == null) ? 0 : localizacion.hashCode());
		result = prime * result + Float.floatToIntBits(longitud);
		result = prime * result + ((periodo == null) ? 0 : periodo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ubicacion other = (Ubicacion) obj;
		if (Float.floatToIntBits(distancia) != Float.floatToIntBits(other.distancia))
			return false;
		if (Float.floatToIntBits(latitud) != Float.floatToIntBits(other.latitud))
			return false;
		if (localizacion == null) {
			if (other.localizacion != null)
				return false;
		} else if (!localizacion.equals(other.localizacion))
			return false;
		if (Float.floatToIntBits(longitud) != Float.floatToIntBits(other.longitud))
			return false;
		if (periodo == null) {
			if (other.periodo != null)
				return false;
		} else if (!periodo.equals(other.periodo))
			return false;
		return true;
	}
	public int compareTo(Ubicacion arg0) {
		int r = 0;
		if (!this.periodo.equals(arg0.getPeriodo()))
			return this.periodo.compareTo(arg0.getPeriodo());
		else if (this.distancia != arg0.getDistancia())
			return this.distancia > arg0.getDistancia() ? 1 : -1;
		else if (this.latitud != arg0.getLatitud())
			return this.latitud > arg0.getLatitud() ? 2 : -2;
		else if (this.longitud != arg0.getLongitud())
			return this.longitud > arg0.getLongitud() ? 3 : -3;
		return r;
	}
}
